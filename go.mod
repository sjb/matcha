module github.com/emersion/matcha

require (
	github.com/alecthomas/chroma v0.5.0
	github.com/danwakefield/fnmatch v0.0.0-20160403171240-cbb64ac3d964
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/dlclark/regexp2 v1.1.6
	github.com/emirpasic/gods v1.9.0
	github.com/jbenet/go-context v0.0.0-20150711004518-d14ea06fba99
	github.com/kevinburke/ssh_config v0.0.0-20180830205328-81db2a75821e
	github.com/kr/pty v1.1.2 // indirect
	github.com/labstack/echo v0.0.0-20170824155736-cec7629194fe
	github.com/labstack/gommon v0.2.1
	github.com/mattn/go-colorable v0.0.9
	github.com/mattn/go-isatty v0.0.4
	github.com/mitchellh/go-homedir v1.0.0
	github.com/pelletier/go-buffruneio v0.2.0
	github.com/russross/blackfriday v2.0.0+incompatible
	github.com/sergi/go-diff v1.0.0
	github.com/shurcooL/octiconssvg v0.0.0-20180602230221-c42b0e3b24d9
	github.com/shurcooL/sanitized_anchor_name v0.0.0-20170918181015-86672fcb3f95 // indirect
	github.com/src-d/gcfg v1.3.0
	github.com/stretchr/testify v1.2.2
	github.com/valyala/bytebufferpool v0.0.0-20160817181652-e746df99fe4a // indirect
	github.com/valyala/fasttemplate v0.0.0-20170224212429-dcecefd839c4 // indirect
	github.com/xanzy/ssh-agent v0.2.0
	golang.org/x/crypto v0.0.0-20180830192347-182538f80094
	golang.org/x/net v0.0.0-20180826012351-8a410e7b638d
	golang.org/x/sys v0.0.0-20180903190138-2b024373dcd9
	golang.org/x/text v0.3.0
	gopkg.in/labstack/echo.v1 v1.4.4
	gopkg.in/src-d/go-billy.v4 v4.2.1
	gopkg.in/src-d/go-git.v4 v4.6.0
	gopkg.in/warnings.v0 v0.1.2
)
